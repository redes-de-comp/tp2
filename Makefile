build:
	@echo "\n\e[1m\e[32m-- Building docker images --\e[22m\e[24m\e[39m"
	docker-compose build

setup:
	@echo "\n\e[1m\e[32m-- Mounting containers --\e[22m\e[24m\e[39m"
	docker-compose up -d

clean:
	@echo "\n\e[1m\e[32m-- Removing containers --\e[22m\e[24m\e[39m"
	docker-compose down

delete_routes:
	@echo "\n\e[1m\e[32m-- Configuring default IPv4 gateway on nodes --\e[22m\e[24m\e[39m"
	docker exec -it ospf_R1 ip route del default
	docker exec -it ospf_R2 ip route del default
	docker exec -it ospf_R3 ip route del default
	docker exec -it ospf_R4 ip route del default
	docker exec -it ospf_R5 ip route del default
	docker exec -it ospf_H1 ip route del default
	docker exec -it ospf_H2 ip route del default
	docker exec -it ospf_H3 ip route del default
	docker exec -it ospf_H4 ip route del default
	docker exec -it ospf_H5 ip route del default

	docker exec -it ospf_H1 ip route add default via 192.168.1.2
	docker exec -it ospf_H2 ip route add default via 192.168.1.2
	docker exec -it ospf_H3 ip route add default via 192.168.1.2
	docker exec -it ospf_H4 ip route add default via 192.168.2.2
	docker exec -it ospf_H5 ip route add default via 192.168.3.2

check_connectivity:
	@echo "\n\e[1m\e[32m-- Checking IPv4 connectivity --\e[22m\e[24m\e[39m"
	docker exec -ti ospf_H1 ping -c 3 192.168.2.5;
	docker exec -ti ospf_H1 ping -c 3 192.168.3.5;
	docker exec -ti ospf_H5 ping -c 3 192.168.1.6;

check_trace:
	@echo "\n\e[1m\e[32m-- Trace route H1 - H4 --\e[22m\e[24m\e[39m"
	docker exec -ti ospf_H1 traceroute 192.168.2.5;
	@echo "\n\e[1m\e[32m-- Trace route H1 - H5 --\e[22m\e[24m\e[39m"
	docker exec -ti ospf_H1 traceroute 192.168.3.5;
	@echo "\n\e[1m\e[32m-- Trace route H4 - H5 --\e[22m\e[24m\e[39m"
	docker exec -ti ospf_H4 traceroute 192.168.3.5;
